-- Setup the GPIO OUTPUT, Output LED High level, the LED will be lighten;
-- Setup the GPIO INPUT, press the button;
-- Test finish
Example and Test Code
#include <wiringPi.h>
#include <pcf8574.h>
 
int LED1[]={7,0,3,12,13,14};
int i;
 
int initIO()
{
    pinMode(7,OUTPUT);
    pinMode(0,OUTPUT);
    pinMode(3,OUTPUT);
    pinMode(12,OUTPUT);
    pinMode(13,OUTPUT);
    pinMode(14},OUTPUT);
}
 
int LED()
{
    for(i=0;i<=6;i++)
    {
        digitalWrite(LED1[i],1);
        delay(500);
        digitalWrite(LED1[i],0);
        delay(500);
    }
}
 
int main()
{
    int val;
    wiringPiSetup();
    pcf8574Setup(200,0x27);
    initIO();
    pinMode(10,INPUT);
    digitalWrite(11,1);
    while(1)
    {
        blink();
        val=digitalRead(10);
        if(val==0)
        {
            LED();
            digitalWrite(11,1);
            i=0;
        }
        else
        blink();
    }
   
}